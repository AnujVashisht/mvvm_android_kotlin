package com.anuj.breweries.core.network

import com.anuj.breweries.business.common.dto.Brewery
import io.reactivex.Single
import retrofit2.http.GET

/**
 * API class for application
 */
interface BreweriesApi {

    @GET("/breweries")
    fun getBreweries(): Single<List<Brewery>>

}
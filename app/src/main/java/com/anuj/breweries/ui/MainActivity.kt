package com.anuj.breweries.ui

import android.os.Bundle
import com.anuj.breweries.R
import com.anuj.breweries.ui.home.HomeFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            replaceFragment(HomeFragment.getInstance(), HomeFragment::class.java.simpleName)
        }
        setSupportActionBar(toolbar)
    }
}

package com.anuj.breweries.business.home.repository

import com.anuj.breweries.db.BreweriesDatabase
import com.anuj.breweries.db.BreweryEntity

class HomeLocalRepository(private val database: BreweriesDatabase) {

    fun insertAll(breweryEntity: List<BreweryEntity>) {
        database.breweryDao().deleteAll()
        database.breweryDao().insertBreweries(breweryEntity)
    }

    fun getLocalBreweries() = database.breweryDao().getAll()
}